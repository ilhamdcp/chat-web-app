import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'


Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    chats: [
      {
          userId: 1,
          userName: "Budi",
          chatHistory: [
              {
                id: 1,
                text: "halo"
              }
          ]
      },
  
      {
          userId: 2,
          userName: "Rina",
          chatHistory: [
              {
                id: 1,
                text: "hai cantik"
              }
          ]
      }
  ],
  currentChat: null
  },
  computed: {
    getAllChats (state) {
      return state.chat
    }
  },
  methods: {
    getCurrentChat(userId) {
        return this.chats.filter(chat => chat.userId === userId)
    }
  },
  mutations: {
    setCurrentChat(state, userId) {
      const filteredChats = state.chats.filter(chat => chat.userId === userId)
      if (filteredChats.length > 0) {
        state.currentChat = filteredChats[0]
      }
    }
  }
})


// const chatData = [
//     {
//         roomId: 1,
//         userId: 1,
//         userName: "Budi",
//         chat: [
//             "halo",
//             "halo juga budsss",
//             "apa kabar broww",
//             "baik, lu apa kabar?",
//             "buruk"
//         ]
//     },

//     {
//         roomId: 2,
//         userId: 2,
//         userName: "Rina",
//         chat: [
//             "hai cantik",
//             "paansih najis",
//             "i love you",
//             "i love you too"
//         ]
//     }
// ]

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
